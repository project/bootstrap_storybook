<?php

/**
 * @file
 * Custom theme hooks.
 */

/**
 * Preprocess variables for all templates.
 * @param $variables
 */
function bootstrap_storybook_preprocess(&$variables) {
  // Inject the current language into every template
  $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
  $variables['language'] = $language;

  global $base_url;
  $variables['base_url'] = $base_url;
}
